import { Runtime } from "./systems/runtime";

const runtime = new Runtime();

module.exports.loop = () => runtime.loop();
