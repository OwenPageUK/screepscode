export const enum RoleType {
    UPGRADER,
    BUILDER,
    HARVESTER,
}
