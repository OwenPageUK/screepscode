export abstract class Role {
    public constructor(protected creep: Creep) {}

    public abstract run(): void;
}
