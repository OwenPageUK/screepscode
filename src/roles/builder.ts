import { Role } from "./base/role";
import { RoleType } from "./base/role_type";

declare global {
    interface CreepMemory {
        role: RoleType;
        building?: boolean;
    }
}

export class Builder extends Role {
    public run(): void {
        if (this.creep.memory.building && this.creep.store[RESOURCE_ENERGY] == 0) {
            this.creep.memory.building = false;
            this.creep.say("🔄 harvest");
        }
        if (!this.creep.memory.building && this.creep.store.getFreeCapacity() == 0) {
            this.creep.memory.building = true;
            this.creep.say("🚧 build");
        }

        if (this.creep.memory.building) {
            var targets = this.creep.room.find(FIND_CONSTRUCTION_SITES);
            if (targets.length) {
                if (this.creep.build(targets[0]) == ERR_NOT_IN_RANGE) {
                    this.creep.moveTo(targets[0], { visualizePathStyle: { stroke: "#ffffff" } });
                }
            }
        } else {
            var sources = this.creep.room.find(FIND_SOURCES);
            if (this.creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                this.creep.moveTo(sources[0], { visualizePathStyle: { stroke: "#ffaa00" } });
            }
        }
    }
}
