import { Role } from "./base/role";
import { RoleType } from "./base/role_type";

declare global {
    interface CreepMemory {
        role: RoleType;
        upgrading?: boolean;
    }
}

export class Upgrader extends Role {

    public run(): void {
        if (this.creep.memory.upgrading && this.creep.store[RESOURCE_ENERGY] == 0) {
            this.creep.memory.upgrading = false;
            this.creep.say("🔄 harvest");
        }

        if (!this.creep.memory.upgrading && this.creep.store.getFreeCapacity(RESOURCE_ENERGY) == 0) {
            this.creep.memory.upgrading = true;
            this.creep.say("⚡ upgrade");
        }

        if (
            this.creep.memory.upgrading &&
            this.creep.upgradeController(this.creep.room.controller as StructureController) == ERR_NOT_IN_RANGE
        ) {
            this.creep.moveTo(this.creep.room.controller as StructureController, {
                visualizePathStyle: { stroke: "#ffffff" },
            });
        } else {
            const sources: Source[] = this.creep.room.find(FIND_SOURCES);

            if (this.creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                this.creep.moveTo(sources[0], {
                    visualizePathStyle: { stroke: "#ffaa00" },
                });
            }
        }
    }
}
