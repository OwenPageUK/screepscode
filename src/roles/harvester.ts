import { Role } from "./base/role";
import { RoleType } from "./base/role_type";

declare global {
    interface CreepMemory {
        role: RoleType;
        state?: State;
    }
}

enum State {
    HARVEST,
    TRANSFER,
}

export class Harvester extends Role {
    private harvest(): void {
        if (this.creep.store.getFreeCapacity() == 0) {
            this.creep.memory.state = State.TRANSFER;
        } else {
            const sources = this.creep.room.find(FIND_SOURCES);

            const target = this.creep.pos.findClosestByPath(sources);

            if (target != null && this.creep.harvest(target) == ERR_NOT_IN_RANGE) {
                this.creep.moveTo(target, { visualizePathStyle: { stroke: "#ffaa00" } });
            }
        }
    }

    private transfer(): void {
        if (this.creep.store.getUsedCapacity() == 0) {
            this.creep.memory.state = State.HARVEST;
        } else {
            const store_targets = this.creep.room.find(FIND_STRUCTURES, {
                filter: (structure) =>
                    (structure.structureType == STRUCTURE_EXTENSION || structure.structureType == STRUCTURE_SPAWN) &&
                    // || structure.structureType == STRUCTURE_CONTAINER
                    structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0,
            });

            if (store_targets.length > 0) {
                const store_target = this.creep.pos.findClosestByPath(store_targets);

                if (store_target != null && this.creep.transfer(store_target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    this.creep.moveTo(store_target, { visualizePathStyle: { stroke: "#ffffff" } });
                }
            } else {
                const build_targets = this.creep.room.find(FIND_CONSTRUCTION_SITES);

                if (false && build_targets.length > 0) {
                    // const build_target = this.creep.pos.findClosestByPath(build_targets);
                    // if (build_target != null && this.creep.build(build_target) == ERR_NOT_IN_RANGE) {
                    //     this.creep.moveTo(build_target, { visualizePathStyle: { stroke: "#ffffff" } });
                    // }
                } else {
                    const upgrade_target = this.creep.room.controller;

                    if (upgrade_target != null && this.creep.upgradeController(upgrade_target) == ERR_NOT_IN_RANGE) {
                        this.creep.moveTo(upgrade_target, { visualizePathStyle: { stroke: "#ffffff" } });
                    }
                }
            }

            // if (this.creep.memory.building && this.creep.store[RESOURCE_ENERGY] == 0) {
            //     this.creep.memory.building = false;
            //     this.creep.say("🔄 harvest");
            // }
            // if (!this.creep.memory.building && this.creep.store.getFreeCapacity() == 0) {
            //     this.creep.memory.building = true;
            //     this.creep.say("🚧 build");
            // }

            // if (this.creep.memory.building) {
            //     var targets = this.creep.room.find(FIND_CONSTRUCTION_SITES);
            //     if (store_targets.length) {
            //         if (this.creep.build(store_targets[0]) == ERR_NOT_IN_RANGE) {
            //             this.creep.moveTo(store_targets[0], { visualizePathStyle: { stroke: "#ffffff" } });
            //         }
            //     }
            // } else {
            //     var sources = this.creep.room.find(FIND_SOURCES);
            //     if (this.creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
            //         this.creep.moveTo(sources[0], { visualizePathStyle: { stroke: "#ffaa00" } });
            //     }
            // }
        }
    }

    public run(): void {
        if (this.creep.memory.state == null) {
            this.creep.memory.state = State.HARVEST;
        }

        if (this.creep.memory.state == State.HARVEST) {
            // const sources = this.creep.room.find(FIND_SOURCES);

            // if (this.creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
            //     this.creep.moveTo(sources[0], { visualizePathStyle: { stroke: "#ffaa00" } });
            // }

            this.harvest();
        } else {
            // const targets = this.creep.room.find(FIND_STRUCTURES, {
            //     filter: (structure) => {
            //         return (
            //             (structure.structureType == STRUCTURE_EXTENSION ||
            //                 structure.structureType == STRUCTURE_SPAWN) &&
            //             structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0
            //         );
            //     },
            // });

            // if (targets.length > 0) {
            //     if (this.creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
            //         this.creep.moveTo(targets[0], { visualizePathStyle: { stroke: "#ffffff" } });
            //     }
            // }
            this.transfer();
        }
    }
}
