import _ from "lodash";
import { RoleType } from "../roles/base/role_type";

export class PopulationManager {
    public run(): void {
        console.log("Running population manager");

        // const upgraders = _.filter(Game.creeps, (creep) => creep.memory.role == RoleType.UPGRADER);

        // if (upgraders.length < 1) {
        //     const name = `Upgrader${Game.time}`;

        //     Game.spawns["Spawn1"].spawnCreep([WORK, CARRY, MOVE], name, { memory: { role: RoleType.UPGRADER } });
        // }

        // const builders = _.filter(Game.creeps, (creep) => creep.memory.role == RoleType.BUILDER);

        // if (builders.length < 1) {
        //     const name = `Builder${Game.time}`;

        //     Game.spawns["Spawn1"].spawnCreep([WORK, CARRY, MOVE], name, { memory: { role: RoleType.BUILDER } });
        // }

        // const harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == RoleType.HARVESTER);

        // if (harvesters.length < 2) {
        //     const name = `Harvester${Game.time}`;

        //     Game.spawns["Spawn1"].spawnCreep([WORK, CARRY, MOVE], name, { memory: { role: RoleType.HARVESTER } });
        // }

        if (_.size(Game.creeps) < 7) {
            const name = `Harvester${Game.time}`;

            Game.spawns["Spawn1"].spawnCreep([WORK, CARRY, MOVE], name, { memory: { role: RoleType.HARVESTER } });
        }

        if (Game.spawns["Spawn1"].spawning) {
            const spawningCreep = Game.creeps[Game.spawns["Spawn1"].spawning.name];
            Game.spawns["Spawn1"].room.visual.text(
                "🛠️" + spawningCreep.memory.role,
                Game.spawns["Spawn1"].pos.x + 1,
                Game.spawns["Spawn1"].pos.y,
                { align: "left", opacity: 0.8 }
            );
        }
    }
}
