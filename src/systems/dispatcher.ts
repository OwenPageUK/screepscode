import { Task } from "../task";

declare global {
    interface Memory {
        tasks: Task[];
    }
}

export class Dispatcher {
    public constructor() {}

    public add_task(task: Omit<Task, "id">): void {}
}
