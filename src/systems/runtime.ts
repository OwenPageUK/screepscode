import { RoleType } from "../roles/base/role_type";
import { Builder } from "../roles/builder";
import { Harvester } from "../roles/harvester";
import { Upgrader } from "../roles/upgrader";
import { PopulationManager } from "./population_manager";

export class Runtime {
    private population_manager: PopulationManager = new PopulationManager();

    public loop() {

        this.population_manager.run();

        for (const name in Game.creeps) {
            const creep = Game.creeps[name];

            switch (creep.memory.role) {
                case RoleType.HARVESTER:
                    new Harvester(creep).run();
                    break;
                case RoleType.UPGRADER:
                    new Upgrader(creep).run();
                    break;
                case RoleType.BUILDER:
                    new Builder(creep).run();
                    break;
            }
        }
    }
}
