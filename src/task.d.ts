export const enum TaskType {}

export interface Task {
    id: unique number;
    action: TaskType;
    target: number;
    requirements: [];
    priority: number;
}
