import typescript from "@rollup/plugin-typescript";

export default {
    input: "src/main.ts",
    plugins: [typescript({ tsconfig: "./tsconfig.json" })],
    output: {
        file: "main.js",
        format: "cjs",
    },
};
